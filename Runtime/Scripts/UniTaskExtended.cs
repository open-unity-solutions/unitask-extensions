using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;


namespace Valhalla.UniTaskExtension
{
	public static class UniTaskExtended
	{
		public static async UniTask Delay(
		float time,
		DelayType delayType = DelayType.DeltaTime,
		PlayerLoopTiming playerLoopTiming = PlayerLoopTiming.Update,
		CancellationToken cancellationToken = default
		)
		{
			await UniTask.Delay((int) (time * 1000), delayType, playerLoopTiming, cancellationToken);
		}


		public static async UniTask DelayPlayMode(
		float time,
		DelayType delayType = DelayType.DeltaTime,
		PlayerLoopTiming playerLoopTiming = PlayerLoopTiming.Update,
		CancellationToken cancellationToken = default
		)
		{
			#if UNITY_EDITOR
			if (Application.isPlaying)
			#endif
				await Delay(time, delayType, playerLoopTiming, cancellationToken);
		}
		
		public static async UniTask DelayFramePlayMode(
		int frames, 
		PlayerLoopTiming playerLoopTiming = PlayerLoopTiming.Update, 
		CancellationToken cancellationToken = default
		)
		{
			#if UNITY_EDITOR
			if (Application.isPlaying)
			#endif
				await UniTask.DelayFrame(frames, playerLoopTiming, cancellationToken);
		}


		public static async UniTask WaitForFixedUpdatePlayMode(CancellationToken cancellationToken = default)
		{
			#if UNITY_EDITOR
			if (Application.isPlaying)
			#endif
				await UniTask.WaitForFixedUpdate(cancellationToken);
		}
	}
}
