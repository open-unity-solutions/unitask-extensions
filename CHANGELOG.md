## [1.4.0]

### Changed
- Rebranded to Valhalla


## [1.3.0]

### Added
- Added frame-related unitasks, which are awaited only in playmode

### Changed
- Renamed `UniTaskPlus` to `UniTaskExtended`


## [1.2.0]

### Added
- CustomYieldInstruction WaitForTask, for getting task results in coroutines

### Changed
- `CHANGELOG.md` reformatted


## [1.1.0]

### Added
- Added `UniTaskPlus` class with float .Delay

### Changed
- Renamed `UniTaskExt` to `UniTaskExtensions`


## [1.0.0]

### Added
- Package created
