## About


## Exception handler extension 

If you run task without `await`, exceptions would not be handled. To get your exceptions, you now can run it with `.Forget(%YourHandler%)`.

So, to save you some precious seconds, this module adds `.ForgetWithHandler()`, that ensures, that all errors will be thrown.

### Usage

```csharp
private void MySyncMethod()
{
	MyAsyncFunction().ForgetWithHandler();
}
```


## Float delay

You can use `UniTask.Delay(int time, ...)` to wait some `int` milliseconds.

Now you can use `UniTaskPlus.Delay(float time, ...)` to wait some `float` seconds.

Why? Maybe to have save API as DOTween have. Or for any other reason.

### Usage

```csharp
private async UniTask MyMethod()
{
    await UniTaskPlus.Delay(0.3f);
} 
```


## WaitForTask

Sometimes you need to run generic UniTask and get it's result. Here is some sugar to make it easier:

```csharp
private IEnumerator MyCoroutine
{
    MyType taskResult = default;
    yield return new WaitForTask<MyType>(MyTaskAsync(), result => taskResult = result);
}
```