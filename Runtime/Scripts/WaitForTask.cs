using System;
using Cysharp.Threading.Tasks;
using UnityEngine;


namespace Valhalla.UniTaskExtension
{
	public class WaitForTask<TResult> : CustomYieldInstruction
	{
		public override bool keepWaiting
			=> !_isDone;


		private readonly UniTask<TResult> _task;
		private readonly Action<TResult> _setter;
		private bool _isDone = false;


		public WaitForTask(UniTask<TResult> task, Action<TResult> setter)
		{
			_task = task;
			_setter = setter;
			
			WaitTaskAsync().ForgetWithHandler();
		}


		private async UniTask WaitTaskAsync()
		{
			_setter.Invoke(await _task);
			_isDone = true;
		}
	}
}
