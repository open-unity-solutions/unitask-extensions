using System;
using Cysharp.Threading.Tasks;


namespace Valhalla.UniTaskExtension
{
	public static class UniTaskExtensions
	{
		private static void HandleAsyncException(Exception e)
		{
			throw e;
		}
		
		
		public static void ForgetWithHandler(this UniTask task)
			=> task.Forget(HandleAsyncException);
		
		
		public static void ForgetWithHandler<T>(this UniTask<T> task)
			=> task.Forget(HandleAsyncException);
	}
}